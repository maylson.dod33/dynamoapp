const express = require("express");
const bodyParser = require('body-parser');
const AWS = require("aws-sdk");
AWS.config.update({ region: 'us-west-2' });
const port = 8080;
const { v4: uuidv4 } = require('uuid');
const app=express();
const client = new AWS.DynamoDB.DocumentClient();
app.use(bodyParser.json());
const tableName = 'Users';

console.log("Creating new user...");

app.get("/usuarios", (req, res) => {
  var params = {
    TableName: tableName
  };

  client.scan(params, (err, data) => {
      if (err) {
          console.log(err);
      } else {
          var items = [];
          for (var i in data.Items)
              items.push(data.Items[i]);

          res.contentType = 'application/json';
          res.send(items);
      }
  });
});

app.get('/usuarios/:name', function(req, res) {

  var params = {
    TableName: tableName,
    Key: {
      Name: req.params.name
    },
  };

  client.get(params, function(err, data) {
    if (err) {
      console.log("Error", err);
    } else {
      res.status(200).json(data.Item);
      console.log("Success", data.Item);
    }
  });
});

app.put("/usuarios", (req, res) => {
  var body = req.body;
  var params = {
      TableName: tableName,
      Item: {
            "Id": uuidv4(),
          "Name": body["name"],
          "LastName": body["lastname"],
          "Age":body["age"]
      }
  };

  client.put(params, (err, data) => {
      if (err) {
          console.error("Unable to add item.");
          console.error("Error JSON:", JSON.stringify(err, null, 2));
      } else {
          console.log("Added item:", JSON.stringify(data, null, 2));
          res.send("User Added")
      }
  });
});

app.post("/usuarios/:name", (req, res) => {
  var body = req.body;
  var params = {
      TableName: tableName,
      Key: {
        Name: req.params.name
      },
      Item: {
          "Id": uuidv4(),
          "Name": body["name"],
          "LastName": body["lastname"],
          "Age":body["age"]
      }
  };

  client.put(params, (err, data) => {
      if (err) {
          console.error("Unable to add item.");
          console.error("Error JSON:", JSON.stringify(err, null, 2));
      } else {
          console.log("Added item:", JSON.stringify(data, null, 2));
          res.send("User Updated")
      }
  });
});

app.delete('/usuarios/:name',function(req, res, next) {
  
  var params = {
    TableName: tableName,
    Key: {
      Name: req.params.name,
    },
  };

  client.delete(params, function (error,data) {
    if (error) {
      console.log(error);
      res.status(400).json({ error: 'Could not delete user' });
    }
    console.log("User Deleted");
    res.status(200).json('User Deleted');
  });
})



app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});